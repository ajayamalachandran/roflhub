set -e
cd roflhub
git pull origin master
sudo docker rmi $(sudo docker images -f dangling=true -q)
# sudo docker rm $(sudo docker ps -qa --no-trunc --filter "status=exited")
sudo docker-compose build
sudo docker-compose up -d
